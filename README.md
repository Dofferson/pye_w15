# Pye W15

Arduino sketch for setting the TX/RX LO frequencies for the Pye W15 sets.

Requires: Si5351 libray from Jason Milldrum <milldrum@gmail.com>  
          https://github.com/etherkit/Si5351Arduino

##Used pins##

Channel selector: digital pins 2 ... 11, active HIGH.
PTT: digital pin 12, active LOW.
CTCSS: digital pin 13.
		  
This sketch uses the S15351 breakout board to generate LO frequencies for 10 selectable channels
(digital pins 2 ... 11). 

Whenever the channel selector switch is changed the Si5351 is instructed to output a signal on a frequency based on
the RX and / or TX frequencies defined in the `channels` array:
  
RX:
  
Output   :  Si5351 CLK0
Frequency: (freq-10.7)/36
   
TX:  
 
Output   :  Si5351 CLK1
Frequency:  freq/32

CTCSS:

Output   : pin 13, PWM


For the PYE W15 the PTT signal is active when pulled low which sets the TX frequency and activates CLK1.

Channel selector pins (digital pins 2 ... 11) are active HIGH meaning they sholuld be pulled low using a pulldown resistor (i.e. 10K).
The PTT selector pin (digital pin 12) is active LOW which means it should be pulled high using a pullup resitor (i.e. 10K).


 
Debug information is echoed on the terminal on 9600 Bd
 
73, Dave Hoebe PA5DOF